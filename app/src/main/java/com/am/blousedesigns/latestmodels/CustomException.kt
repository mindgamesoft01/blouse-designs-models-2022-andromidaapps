package com.am.blousedesigns.latestmodels

class CustomException(message: String) : Exception(message)
